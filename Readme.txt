AUTHORS：Y.J.Weng
create on 2021.08.16

※注意事項：以下各程式有順序，順序錯誤將造成程式錯誤。

1.PaperCommon：共用模組，含有許多共用函式。

2.ImportData：將資料寫入mongoDB，分成單筆寫入與匯入json檔。

3.ErrorTimeProcess：處理有問題的gameTime。

4.CreateThreeCollections：論文生命，創建3個主要的collection - AccuTime, FirstPass, FirstRecord。AccuTime計算累計時間的正確率、log時間、Z值；FirstPass記錄每一關直到破關前的遊戲記錄；FirstRecord記錄每一關第1次遊戲的記錄。

5.EloRating：計算關卡與玩家的Elo積分

6.Kmeans：對關卡及玩家的Elo積分進行分級

7.Insert_AccuTime：計算AccuTime中gameTime及Log gameTime的Z值，各種敘述性統計資料並寫入Sec_sta。

8.Insert_FirstRecord：計算FirstRecord中gameTime及Log gameTime的Z值，各種敘述性統計資料。

9.AccuTime_Statistic：將AccuTime的資料進行統計，畫圖用。

10.FirstRecord_Statistic：將FirstRecord的資料進行統計，畫圖用。

11.執行Paper.py會自動按順序執行所有以上程式，但是需分成2階段：
(1)第一階段執行到Kmeans，此時會繪圖讓研究者自行判斷最佳分群組數
(2)第二階段研究者須先將Kmeas.main前面的部分(就是第一階段執行的部分)註解掉，避免重複執行。再來研究者須在Paper.py中的Kmeans.main的第2個參數填入最佳分群組數，再執行一次Paper.py。

12.PaperPainting和Painting是將執行完成的結果視覺化，畫成圖。